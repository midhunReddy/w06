# w06 Chat Example

A simple chat demo using node.js, express, and socket.io
@author Midhun Kumar
@version 2/15/2018
Yours Sincerly

## How to use

Open a command window in your c:\44563\m06 folder.

Run npm install to install all the dependencies in the package.json file.

Run node server.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node server.js
```

Point your browser to `http://localhost:8081`. 


